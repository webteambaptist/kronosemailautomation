﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using KronosEmailAutomation.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NLog;
using OfficeOpenXml;
using ILogger = KronosEmailAutomation.Interfaces.ILogger;

namespace KronosEmailAutomation.Services
{
    public class FileProcessor : ILogger
    {
        private readonly IConfiguration _config;
        private readonly NotificationService _notificationService;
        private readonly LoggerService _logger;
        public FileProcessor(IConfiguration config, NotificationService notificationService)
        {
            _config = config;
            _notificationService = notificationService;
            _logger = CreateLogger();
        }
        /// <summary>
        /// This processes the data from the source excel spreadsheet
        /// </summary>
        /// <param name="fileLocation">path to the file</param>
        /// <returns>List of Rows from spreadsheet</returns>
        public List<RowModel> GetDataFromFile(string fileLocation)
        {
            var resultList = new List<RowModel>();
            using var p = new ExcelPackage(fileLocation);
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            //get the first worksheet in the workbook
            var firstSheet = p.Workbook.Worksheets[0];
            var colCount = firstSheet.Dimension.End.Column;  //get Column Count
            var rowCount = firstSheet.Dimension.End.Row;     //get row count
            
            for (var row = 2; row <= rowCount; row++)
            {
                try
                {
                    var r = firstSheet.Cells[string.Format("{0}:{0}", row)];

                    var isEmpty = r.All(x => string.IsNullOrEmpty(x.Text));
                    if (isEmpty)
                    {
                        //Log empty row
                        continue;
                    }
                    var rowModel = new RowModel();
                    for (var col = 1; col <= colCount; col++)
                    {
                        try
                        {
                            //Console.WriteLine(" Row:" + row + " column:" + col + " Value:" + firstSheet.Cells[row, col].Value.ToString()?.Trim());
                            switch (col)
                            {
                                case 1:
                                    var rowResult = firstSheet.Cells[row, col].Text.ToString();
                                    rowModel.WeekEnding = rowResult;//firstSheet.Cells[row, col].Value?.ToString();
                                    break;
                                case 2:
                                    rowModel.Id = firstSheet.Cells[row, col].Text.ToString();
                                    break;
                                //case 3:
                                //    rowModel.PayrollHours = firstSheet.Cells[row, col].Text.ToString();
                                //    break;
                                //case 4:
                                //    rowModel.KronosHours = firstSheet.Cells[row, col].Text.ToString();
                                //    break;
                                //case 5:
                                //    rowModel.StndHours = firstSheet.Cells[row, col].Text.ToString();
                                //    break;
                                case 3:
                                    rowModel.LastName = firstSheet.Cells[row, col].Text.ToString();
                                    break;
                                case 4:
                                    rowModel.FirstName = firstSheet.Cells[row, col].Text.ToString();
                                    break;
                                case 5:
                                    rowModel.FullPart = firstSheet.Cells[row, col].Text.ToString();
                                    break;
                                case 6:
                                    rowModel.EmplClass = firstSheet.Cells[row, col].Text.ToString();
                                    break;
                                case 7:
                                    rowModel.Status = firstSheet.Cells[row, col].Text.ToString();
                                    break;
                                //case 11:
                                //    rowModel.LastStart = firstSheet.Cells[row, col].Text.ToString();
                                //    break;
                                case 8:
                                    rowModel.TermDate = firstSheet.Cells[row, col].Text.ToString();
                                    break;
                                //case 13:
                                //    rowModel.LeaveType = firstSheet.Cells[row, col].Text.ToString();
                                //    break;
                                case 9:
                                    rowModel.Co = firstSheet.Cells[row, col].Text.ToString();
                                    break;
                                case 10:
                                    rowModel.Unit = firstSheet.Cells[row, col].Text.ToString();
                                    break;
                                case 11:
                                    rowModel.DeptId = firstSheet.Cells[row, col].Text.ToString();
                                    break;
                                case 12:
                                    rowModel.DeptName = firstSheet.Cells[row, col].Text.ToString();
                                    break;
                                //case 16:
                                //    rowModel.Location = firstSheet.Cells[row, col].Text.ToString();
                                //    break;
                                //case 19:
                                //    rowModel.JobCode = firstSheet.Cells[row, col].Text.ToString();
                                //    break;
                                case 13:
                                    rowModel.JobTitle = firstSheet.Cells[row, col].Text.ToString();
                                    break;
                                case 14:
                                    rowModel.Manager1EmployeeId = firstSheet.Cells[row, col].Text.ToString();
                                    break;
                                case 15:
                                    rowModel.Manager1UserId = firstSheet.Cells[row, col].Text.ToString();
                                    break;
                                case 16:
                                    rowModel.Manager1Name = firstSheet.Cells[row, col].Text.ToString();
                                    break;
                                case 17:
                                    rowModel.Manager2EmployeeId = firstSheet.Cells[row, col].Text.ToString();
                                    break;
                                case 18:
                                    rowModel.Manager2UserId = firstSheet.Cells[row, col].Text.ToString();
                                    break;
                                case 19:
                                    rowModel.Manager2Name = firstSheet.Cells[row, col].Text.ToString();
                                    break;
                                case 20:
                                    rowModel.Email = firstSheet.Cells[row, col].Text.ToString();
                                    break;
                            //    case 28:
                            //        rowModel.PayRule = firstSheet.Cells[row, col].Text.ToString();
                            //        break;
                            //    case 29:
                            //        rowModel.OrgRelation = firstSheet.Cells[row, col].Text.ToString();
                            //        break;
                            }
                        }
                        catch (Exception e)
                        {
                            _logger.WriteError(
                                $"Exception occurred inner loop :: GetDataFromFile :: Column:{col} :: {e.Message}");
                        }

                    }
                    
                    
                    resultList.Add(rowModel);
                }
                catch (Exception e)
                {
                    _logger.WriteError($"Exception occurred in outer loop :: GetDataFromFile {e.Message} :: Row{row}");
                }
                
            }

            return resultList;
        }
        /// <summary>
        /// Writes the results to a CSV file that will later be emailed
        /// </summary>
        /// <param name="model">Incoming model that contains the data for the CSV file</param>
        /// <param name="directory">Path to where CSV will be saved </param>
        /// <returns>CSV Path</returns>
        public string WriteToCsv(List<RowModel> model, string directory)
        {
            try
            {
                var deptName = "";
                RowModel? first = model.FirstOrDefault();

                if (first != null)
                {
                    deptName = first.DeptId;
                }

                if (string.IsNullOrEmpty(deptName)) return null;

                var outputCsvPath = directory + deptName.ToString() + ".csv";

                // Append to the file.
                var config = new CsvConfiguration(CultureInfo.InvariantCulture)
                {
                    // Don't write the header again.
                    HasHeaderRecord = true,
                    Delimiter = ",",
                    IgnoreBlankLines = true
                };
                using var writer = new StreamWriter(outputCsvPath);
                using var csv = new CsvWriter(writer, config);

                csv.WriteRecords(model);
                csv.Flush();
                return outputCsvPath;
            }
            catch (Exception e)
            { 
                _logger.WriteError($"Exception :: WriteToCsv :: {JsonConvert.SerializeObject(model)}:: {e.Message}");
                throw;
            }
        }
        /// <summary>
        /// This processes the sorted list and sends out emails to managers
        /// </summary>
        /// <param name="resultList">Sorted list passed in</param>
        public void ProcessSortedList(List<RowModel> resultList)
        {
            string deptId = null;
            var currentDepartment = new List<RowModel>();
            var sortedResultList = resultList.OrderBy(x => x.DeptId).ToList();
            for (var rownum = 0; rownum < sortedResultList.Count; rownum++)
            {
                var row = sortedResultList[rownum];
                //}
            //    foreach (var row in sortedResultList)
            //{
                try
                {
                    // dept not set (must be first row)
                    if (deptId == null)
                    {
                        deptId = row.DeptId;
                    }
                    // same as dept
                    if (row.DeptId == deptId)
                    {
                        currentDepartment.Add(row);
                        if (rownum == sortedResultList.Count-1)
                        {
                            SendMail(currentDepartment, row.DeptId);
                        }
                    }
                    // add current row
                    else
                    {
                        // new department
                        SendMail(currentDepartment, deptId);
                        // set current dept 
                        deptId = row.DeptId;
                        // clear list and add current row to list
                        currentDepartment = new List<RowModel> { row };
                        // if last row send it
                        if (rownum == sortedResultList.Count - 1)
                        {
                            SendMail(currentDepartment, deptId);// send this last one
                        }
                    }
                }
                catch (Exception e)
                {
                    _logger.WriteError($"Exception :: ProcessSortedList :: {e.Message}");
                }
                
            }
        }
        /// <summary>
        /// Creates an instance of Logger
        /// </summary>
        /// <returns></returns>
        public LoggerService CreateLogger()
        {
            var logger = new LoggerService("FileProcessor");
            return logger;
        }

        private void SendMail(List<RowModel> currentDepartment, string deptId)
        {
            try
            {
                var csvFilePath = WriteToCsv(currentDepartment, _config.GetSection("FilePath").Value);
                // now send out email
                var emailBody = _config.GetSection("EmailBody").Value;
                var subject = _config.GetSection("Subject").Value;
                var emailTo = currentDepartment.First(x => x.Manager1UserId!=null).Manager1UserId + "@bmcjax.com";
                var emailCc = currentDepartment.First(x => x.Manager2UserId!=null).Manager2UserId + "@bmcjax.com";
                _notificationService.SendMail(emailBody, emailTo, emailCc, csvFilePath, subject);
                
            }
            catch (Exception e)
            {
                _logger.WriteError($"Exception SendMail:: DeptID: ({deptId}) :: {e.Message}");
            }
        }
    }
}
