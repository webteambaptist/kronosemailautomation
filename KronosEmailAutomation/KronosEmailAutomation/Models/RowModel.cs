﻿using System;

namespace KronosEmailAutomation.Models
{
    public class RowModel
    {
        public string WeekEnding { get; set; }
        public string Id { get; set; }
        //public string PayrollHours { get; set; }
        //public string KronosHours { get; set; }
        //public string StndHours { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string FullPart { get; set; }
        public string EmplClass { get; set; }
        public string Status { get; set; }
        //public string LastStart { get; set; }
        public string TermDate { get; set; }
        //public string LeaveType { get; set; }
        public string Co { get; set; }
        public string Unit { get; set; }
        public string DeptId { get; set; }
        public string DeptName { get; set; }
        //public string Location { get; set; }
        //public string JobCode { get; set; }
        public string JobTitle { get; set; }
        public string Manager1EmployeeId { get; set; }
        public string Manager1UserId { get; set; }
        public string Manager1Name { get; set; }
        public string Manager2EmployeeId { get; set; }
        public string Manager2UserId { get; set; }
        public string Manager2Name { get; set; }
        public string Email { get; set; }
        //public string PayRule { get; set; }
        //public string OrgRelation { get; set; }

    }
}
