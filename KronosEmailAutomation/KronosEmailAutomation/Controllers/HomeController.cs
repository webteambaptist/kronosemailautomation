﻿using KronosEmailAutomation.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using KronosEmailAutomation.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing.Matching;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using ILogger = KronosEmailAutomation.Interfaces.ILogger;

namespace KronosEmailAutomation.Controllers
{
    public class HomeController : Controller, ILogger
    {
        private readonly LoggerService _logger;
        private readonly IConfiguration _config;
        private readonly FileProcessor _fileProcessor;
        private readonly AdService _adService;

        public HomeController(IConfiguration config, IHttpContextAccessor accessor)
        {
            _config = config;
            var notificationService = new NotificationService(_config);
            _fileProcessor = new FileProcessor(_config, notificationService);
            _logger = CreateLogger();
            _adService = new AdService(accessor, config);
        }

        public IActionResult Index()
        {
            ViewBag.Complete = "";
            var user = _adService.GetUserName();
            var userName = _adService.CleanUsername(user);
            var access = _adService.HasAccess(userName);
            return !access ? View("NoAccess") : View();
        }
        /// <summary>
        /// main Run animation process (when button pressed)
        /// </summary>
        /// <returns>Back to Index</returns>
        public IActionResult RunAutomation()
        {
            try
            {
                var inputFile = _config.GetSection("FileLocation").Value;
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                // get data from file
                var resultList = _fileProcessor.GetDataFromFile(inputFile);
            
                // process sorted data
                _fileProcessor.ProcessSortedList(resultList);
            
                ViewBag.Complete = "Automation complete";
                return View("Index");
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = $"Exception occurred in RunAutomation:: {e.Message}";
                _logger.WriteError($"Exception occurred in RunAutomation:: {e.Message}" );
                return RedirectToAction("Error");
            }
            
        }
        /// <summary>
        /// Error Page
        /// </summary>
        /// <returns></returns>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            var error = ViewBag.ErrorMessage;
            return View(new ErrorViewModel { ErrorMessage = error });
        }
        /// <summary>
        /// Creates an instance of the Logger
        /// </summary>
        /// <returns></returns>
        public LoggerService CreateLogger()
        {
            var logger = new LoggerService("Home");
            return logger;
        }
    }
}
