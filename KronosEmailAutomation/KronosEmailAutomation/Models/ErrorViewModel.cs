using System;

namespace KronosEmailAutomation.Models
{
    public class ErrorViewModel
    {
        public string ErrorMessage { get; set; }
    }
}
