﻿using KronosEmailAutomation.Services;

namespace KronosEmailAutomation.Interfaces
{
    /// <summary>
    /// This interface is used to standardize the new logger instance.
    /// This forces you to always create a logger and pass in the logger name before using it
    /// </summary>
    public interface ILogger
    {

        /// <summary>
        /// Interface Method declaration
        /// </summary>
        /// <returns></returns>
        public LoggerService CreateLogger();
    }
}
