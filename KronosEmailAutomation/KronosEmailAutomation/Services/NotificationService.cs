﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using KronosEmailAutomation.Interfaces;
using KronosEmailAutomation.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace KronosEmailAutomation.Services
{
    public class NotificationService : ILogger
    {
        private readonly LoggerService _loggerService;
        private readonly IConfiguration _config;
        /// <summary>
        /// Constructor that creates an instance of the Notification Service
        /// </summary>
        /// <param name="config">This is the config created at startup. This should be passed in</param>
        public NotificationService(IConfiguration config)
        {
           _loggerService = CreateLogger();
           _config = config;
        }
        
        /// <summary>
        /// Creates and instanc of the logger service
        /// </summary>
        /// <returns></returns>
        public LoggerService CreateLogger()
        {
            var logger = new LoggerService("NotificationService");
            return logger;
        }
        /// <summary>
        /// This sends email to recipient
        /// </summary>
        /// <param name="body">body of email</param>
        /// <param name="to">recipient</param>
        /// <param name="cc">CC in email</param>
        /// <param name="attachment">Attachment</param>
        /// <param name="subject">Email Subject</param>
        /// <returns></returns>
        public void SendMail(string body, string to, string cc, string attachment, string subject)
        {
            var from = _config.GetSection("MailFrom").Value;
            try

            {
                var addressFrom = new MailAddress(from);
                var smtp = new SmtpClient
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Host = "smtp.bmcjax.com",
                    Port = 25,
                    EnableSsl = false
                };

                var mailMessage = new MailMessage()
                {
                    From = addressFrom,
                    Body = body,
                    IsBodyHtml = true,
                    Subject = subject
                };

                if (to.Contains(";"))
                {
                    var splitTo = to.Split(";");
                    foreach (var emailAddress in splitTo)
                    {
                        mailMessage.To.Add(emailAddress);
                    }
                }
                else
                {
                    mailMessage.To.Add(to);
                }
               
                if (cc != null)
                {
                    if (cc.Contains(";"))
                    {
                        var splitCC = cc.Split(";");
                        foreach (var emailAddress in splitCC)
                        {
                            mailMessage.CC.Add(emailAddress);
                        }
                    }
                    else
                    {
                        mailMessage.CC.Add(cc);
                    }
                }

                using var stream = new MemoryStream();
                using var writer = new StreamWriter(stream);
                using var mailClient = smtp;
                using var message = mailMessage;
                writer.WriteLine(attachment);
                writer.Flush();
                stream.Position = 0;     // read from the start of what was written

                message.Attachments.Add(new Attachment(attachment));

                mailClient.Send(message);
            }
            catch (Exception ex)
            {
                _loggerService.WriteError("Exception :: SendMail :: occurred :: Details :: " + ex.Message + " " + ex.InnerException??"");
            }
        }
    }
}
