﻿using System;
using System.DirectoryServices;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using KronosEmailAutomation.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace KronosEmailAutomation.Services
{
    public class AdService : ILogger
    {
        private readonly IHttpContextAccessor _accessor;
        private readonly LoggerService _loggerService;
        private readonly IConfiguration _config;
        public AdService(IHttpContextAccessor httpContextAccessor, IConfiguration config)
        {
            _accessor = httpContextAccessor;
            _loggerService = CreateLogger();
            _config = config;
        }
        /// <summary>
        /// Returns the IPrincipal for the logged in user
        /// </summary>
        public IPrincipal CurrentUser
        {
            get
            {
                var context = _accessor.HttpContext;
                return context != null ? context.User : ClaimsPrincipal.Current;
            }
        }
        /// <summary>
        /// Determines if the logged in user is in the role specified
        /// </summary>
        /// <param name="role">Role</param>
        /// <returns>bool (yes/no) in role</returns>
        public bool IsInRole(string role)
        {
            var isInRole = false;
            var user = CurrentUser;
            if (user != null)
            {
                isInRole = user.IsInRole(role);
            }
            return isInRole;
        }
        public LoggerService CreateLogger()
        {
            var logger = new LoggerService("ADService");
            return logger;
        }
        /// <summary>
        /// Removes domain and slashes from username
        /// </summary>
        /// <param name="user">Logged in user</param>
        /// <returns></returns>
        public string CleanUsername(string user)
        {
            try
            {
                var findSlash = user.IndexOf("\\", StringComparison.Ordinal);
                user = user.Substring(findSlash + 1);
                return user.ToUpper();
            }
            catch (Exception e)
            {
                _loggerService.WriteError($"Exception :: CleanUserName :: {e.Message}");
                return null;
            }
            
        }
        /// <summary>
        /// Determines who has access to this screen
        /// </summary>
        /// <param name="userName">Logged in user</param>
        /// <returns></returns>
        public bool HasAccess(string userName)
        {
            var allowed = IsInRole("BHSQL Web Team Developers");;
            if (allowed) return true;
           
            var allowedUsers = _config.GetSection("UserAccessList").Value.Split(",");
            if (allowedUsers.Any(user => user.ToUpper().Equals(userName.ToUpper())))
            {
                allowed = true;
            }

            return allowed;
        }
        /// <summary>
        /// Gets logged in user
        /// </summary>
        /// <returns>username</returns>
        public string GetUserName()
        {
            try
            {
                return _accessor.HttpContext?.User.Identity?.Name;
            }
            catch (Exception e)
            {
                _loggerService.WriteError("Exception :: GetUserName occurred :: Details :: " + e.Message + " " + e.InnerException??"");
                throw;
            }
        }
    }
}
