﻿namespace KronosEmailAutomation.Models
{
    public class Mail
    {
        public string Subject { get; set; }
        public bool IsBodyHtml { get; set; }
        public string Body { get; set; }
        public string To { get; set; }
        public string From { get; set; }
    }
}
